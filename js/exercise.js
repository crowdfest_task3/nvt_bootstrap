var userid = null; // user id
var eid = null; // exercise id
var etype = null; // exercise type
var username = null; // username
var points = 0; // points earned

// etype = 'RelatedTo', 'AtLocation', 'PartOf',

$(document).ready(function () {
    $("#points-label").hide();
    //$("#prev-res-label").hide();
    $("#all-points-list").hide();
    $("#question").val('');
    $('#partof').tooltip({
        title: "Relation exercise",
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner large"></div></div>'
    });

    var new_user = true;
    // get user name and id from Cookies
    var remember = Cookies.get('remember');
    username = Cookies.get('username');
    userid = Cookies.get('userid');
    // If they don't exist, generate them
    if ((remember == false) || (username == null) || (userid == null))
    {
        // if not, generate user and set cookies
        userid = uuidv4();
        username = generate_name();
        Cookies.set('userid', userid, {expires: 5 * 365});
        Cookies.set('username', username, {expires: 5 * 365});
        Cookies.set('remember', 'true', {expires: 5 * 365});
    }
    // check if user already exists in the database
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
        data: {
            'method': 'get_user_info',
            'userid': userid,
        },
        beforeSend: function () {
        },
        success: function (data) {
            // user exists
            new_user = false;
        }
    });
    // if not, add it to the database
    if (new_user)
    {
        $.ajax({
            type: 'POST',
            crossDomain: true,
            url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
            data: {
                'method': 'add_user',
                'userid': userid,
                'uname': username
                        //'etype': relation
                        // level
                        // relation
            },
            beforeSend: function () {
            },
            success: function (data) {

            }
        });
    }

    //get leaderboards updated, 3 top players 1 player above your rank, your rank, and a player below you
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
        data: {
            'method': 'get_leaderboard'
                    //'etype': relation
                    // level
                    // relation
        },
        beforeSend: function () {
        },
        success: function (data) {
            for (let i = 1; i <= 3; i++){
              let user_name = data[i-1].uname;
              let earned_points = data[i-1].earned_points;
              let potential_points = data[i-1].potential_points;
              let badges = data[i-1].badges;
              document.getElementById('user_name_' + i).innerHTML= user_name;
              document.getElementById('points_' + i).innerHTML= earned_points;
              document.getElementById('potential_' + i).innerHTML= potential_points;
              document.getElementById('badges_' + i).innerHTML= badges;
            }

            for(let i=0; i<data.length; i++){
              if(userid == data[i].userid)
                if(data[i].rank < 5)
                {
                  for(let j = 4; j <= 6; j++)
                  {
                    let rank = data[j-1].rank;
                    let user_name = data[j-1].uname;
                    let earned_points = data[j-1].earned_points;
                    let potential_points = data[j-1].potential_points;
                    let badges = data[j-1].badges;
                    document.getElementById('rank_' + j).innerHTML = rank;
                    document.getElementById('user_name_' + j).innerHTML= user_name;
                    document.getElementById('points_' + j).innerHTML= earned_points;
                    document.getElementById('potential_' + j).innerHTML= potential_points;
                    document.getElementById('badges_' + j).innerHTML= badges;
                  }
                }
                else
                {
                  let k = -1;
                  for(let j = 4; j <= 6; j++)
                  {
                      let rank = data[i+k].rank;
                      let user_name = data[i+k].uname;
                      let earned_points = data[i+k].earned_points;
                      let potential_points = data[i+k].potential_points;
                      let badges = data[i+k].badges;
                      k++;
                      document.getElementById('rank_' + j).innerHTML = rank;
                      document.getElementById('user_name_' + j).innerHTML= user_name;
                      document.getElementById('points_' + j).innerHTML= earned_points;
                      document.getElementById('potential_' + j).innerHTML= potential_points;
                      document.getElementById('badges_' + j).innerHTML= badges;
                      // if (data[i+k] == null)
                      //   break;
                  }
                }
                for(let i = 1; i <= 6; i++)
                {
                  if(document.getElementById('user_name_' + i).innerHTML == username){
                    document.getElementById('user_name_' + i).style.backgroundColor = '#FFFF99';
                    document.getElementById('rank_' + i).style.backgroundColor = '#FFFF99';
                    document.getElementById('points_' + i).style.backgroundColor = '#FFFF99';
                    document.getElementById('potential_' + i).style.backgroundColor = '#FFFF99';
                    document.getElementById('badges_' + i).style.backgroundColor = '#FFFF99';
                  }

                }
            }

            //alert("You've got "+ unreal_points + " potential points. Awesome!");
            $('#all-points-list').show();
        }
    });

});



// $("#partof").on("click","",generate_exercise())
//document.getElementById('partof').onclick = generate_exercise();

$(document).on("click", "#partof", function ()
{
    eid = get_exercise(userid, 'PartOf');
});

$(document).on("click", "#location", function ()
{
    eid = get_exercise(userid, 'AtLocation');
});

$(document).on("click", "#relation", function ()
{
    eid = get_exercise(userid, 'RelatedTo');
});

// $(document).on("click","#partof", function()
//
// {
//
// $.ajax({
//            type: 'POST',
//            crossDomain: true,
//            url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
//            data: {
//                'method': 'get_exercise',
//                'userid': userid
//                // level
//                // relation
//            },
//            beforeSend: function () {
//            },
//            success: function (data) {
//                    var exercise = data.exercise;
//                    eid = data.eid;
//                    document.getElementById('question-label').innerHTML = exercise;
//            }
//        });
// }
// );

// $(document).on("focus",'#question'), function ()
// {
//     document.getElementById('points-label').hide();
// }

$(document).on("click", "#answer", function (event)
{
    event.preventDefault();
    var answer = document.getElementById('question').value;
    if (answer != "") {
        $.ajax({
            type: 'POST',
            crossDomain: true,
            url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
            data: {
                'method': 'store_response',
                'userid': userid,
                'eid': eid,
                'response': answer
            },
            beforeSend: function () {
            },
            success: function (data) {
                var points = data.points;
                // points==0 if it is a duplicate answer
                if ((points == 0) || (points < 0))
                {
                    $("#points-button").removeClass("alert-success");
                    $("#points-button").removeClass("alert-warning");
                    $("#points-button").removeClass("alert-danger");
                    $("#points-button").addClass("alert-danger");
                    $('#points-label').show();

                    document.getElementById('points-button').innerHTML = "<i class='fa fa-bug' aria-hidden='true'></i> You've already given this answer before.";
                    //$("#points-label").append('<button style="margin-right:10px; margin-top:10px;" type="button"  class="alert alert-primary" role="alert">' + "Thanks for answering. We'll get back to you when we've processed it." + '</button>');
                    $("#points-label").delay(2000).fadeOut('slow');
                    // document.getElementById('points-label').innerHTML = "Thanks for answering. We'll get back to you when we've processed it.";
                }
                //points == null if it is a potential point
                else if (points == null) {


                    $("#points-button").removeClass("alert-success");
                    $("#points-button").removeClass("alert-warning");
                    $("#points-button").removeClass("alert-danger");
                    $("#points-button").addClass("alert-success");
                    $('#points-label').show();
                    document.getElementById('points-button').innerHTML = "<i class='fa fa-check' aria-hidden='true'></i> Thanks for answering. We'll get back to you when we've processed it.";
                    $("#points-label").delay(2000).fadeOut('slow');


                } else if (points == 1)
                {
                    $("#points-button").removeClass("alert-success");
                    $("#points-button").removeClass("alert-warning");
                    $("#points-button").removeClass("alert-danger");
                    $("#points-button").addClass("alert-success");
                    $('#points-label').show();
                    document.getElementById('points-button').innerHTML = "<i class='fa fa-check' aria-hidden='true'></i> Good job! You've earned " + points + " point.";
                    //$("#points-label").append('<button style="margin-right:10px; margin-top:10px;" type="button"  class="alert alert-primary" role="alert">' + "Good job! You've earned " + points + " point." + '</button>');
                    $("#points-label").delay(2000).fadeOut('slow');
                    // document.getElementById('points-label').innerHTML = "Good job! You've earned " + points + " point.";
                } else
                {
                    $("#points-button").removeClass("alert-success");
                    $("#points-button").removeClass("alert-warning");
                    $("#points-button").removeClass("alert-danger");
                    $("#points-button").addClass("alert-success");
                    $('#points-label').show();
                    document.getElementById('points-button').innerHTML = "<i class='fa fa-check' aria-hidden='true'></i> Good job! You've earned " + points + " points.";
                    //$("#points-label").append('<button style="margin-right:10px; margin-top:10px;" type="button"  class="alert alert-primary" role="alert">' + "Good job! You've earned " + points + " points." + '</button>');
                    $("#points-label").delay(2000).fadeOut('slow');
                    //document.getElementById('points-label').innerHTML = "Good job! You've earned " + points + " points.";
                }
                // clean text field and get next question
                document.getElementById('answer-form').reset();
                eid = get_exercise(userid, 'RelatedTo');
                // $.ajax({
                //            type: 'POST',
                //            crossDomain: true,
                //            url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
                //            data: {
                //                'method': 'get_exercise',
                //                'userid': userid
                //                // level
                //                // relation
                //            },
                //            beforeSend: function () {
                //            },
                //            success: function (data) {
                //                    var exercise = data.exercise;
                //                    eid = data.eid;
                //                    document.getElementById('question-label').innerHTML = exercise;
                //            }
                //        });
                //document.getElementById('question-label').innerHTML = exercise;
                // http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php?method=store_response&userid=1&eid=5&response=kolokotronis
            }
        });
    }
});

$(document).on("click", "#dontknow", function (event)
{
// test etype close/open
    event.preventDefault();
if (etype == 'open')
{
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
        data: {
            'method': 'get_random_response',
            'userid': userid,
            'eid': eid
        },
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            var randomresponse = data.response;

            // points==0 if it is a duplicate answer
            $("#points-button").removeClass("alert-success");
            $("#points-button").removeClass("alert-warning");
            $("#points-button").removeClass("alert-danger");
            $("#points-button").addClass("alert-warning");
            $('#points-label').show();
            document.getElementById('points-button').innerHTML = "<i class='fa fa-question-circle' aria-hidden='true'></i> Don't give up! Try inserting: <b>" + randomresponse + "</b>";
        }
    });
}
else if (etype == 'close')
{
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
        data: {
            'method': 'store_close_response',
            'userid': userid,
            'eid': eid,
            'response': '0'
        },
        beforeSend: function () {
        },
        success: function (data) {
            var points = data.points;
            var correct_response = data.response;

            $("#points-button").removeClass("alert-success");
            $("#points-button").removeClass("alert-warning");
            $("#points-button").removeClass("alert-danger");
            $("#points-button").addClass("alert-danger");
            if (correct_response == '1')
            {
                document.getElementById('points-button').innerHTML = "<i class='fa fa-bug' aria-hidden='true'></i> Don't give up! The correct answer was YES.";
            }
            else
            {
                document.getElementById('points-button').innerHTML = "<i class='fa fa-bug' aria-hidden='true'></i> Don't give up! The correct answer was NO.";
            }
            $('#points-label').show();
            //$("#points-label").append('<button style="margin-right:10px; margin-top:10px;" type="button"  class="alert alert-primary" role="alert">' + "Thanks for answering. We'll get back to you when we've processed it." + '</button>');
            $("#points-label").delay(2000).fadeOut('slow');
            // document.getElementById('points-label').innerHTML = "Thanks for answering. We'll get back to you when we've processed it.";

            // clean text field and get next question
            document.getElementById('answer-form').reset();
            eid = get_exercise(userid, 'RelatedTo');
            //$("#points-label").delay(4000).fadeOut('slow');
            //$("#question").val(randomresponse);
            // clean text field and get next question
            //        document.getElementById('answer-form').reset();
            //        eid = get_exercise(userid,'partof');

            //document.getElementById('question-label').innerHTML = exercise;
            // http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php?method=store_response&userid=1&eid=5&response=kolokotronis
        }
    });


    //
    // $.ajax({
    //     type: 'POST',
    //     crossDomain: true,
    //     url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
    //     data: {
    //         'method': 'get_random_response',
    //         'userid': userid,
    //         'eid': eid
    //     },
    //     beforeSend: function () {
    //     },
    //     success: function (data) {
    //         console.log(data);
    //         var randomresponse = data.response;
    //
    //         // points==0 if it is a duplicate answer
    //         $("#points-button").removeClass("alert-success");
    //         $("#points-button").removeClass("alert-warning");
    //         $("#points-button").removeClass("alert-danger");
    //         $("#points-button").addClass("alert-warning");
    //         $('#points-label').show();
    //         document.getElementById('points-button').innerHTML = "<i class='fa fa-question-circle' aria-hidden='true'></i> Don't give up! Try inserting: <b>" + randomresponse + "</b>";
    //     }
    // });
}

});

$(document).on("click", "#no", function (event)
{
    event.preventDefault();
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
        data: {
            'method': 'store_close_response',
            'userid': userid,
            'eid': eid,
            'response': '-1'
        },
        beforeSend: function () {
        },
        success: function (data) {
            var points = data.points;
            // points==0 if it is a duplicate answer
            if ((points == 0) || (points < 0))
            {
                $("#points-button").removeClass("alert-success");
                $("#points-button").removeClass("alert-warning");
                $("#points-button").removeClass("alert-danger");
                $("#points-button").addClass("alert-danger");
                $('#points-label').show();

                document.getElementById('points-button').innerHTML = "<i class='fa fa-bug' aria-hidden='true'></i> Wrong! The correct answer was YES.";
                //$("#points-label").append('<button style="margin-right:10px; margin-top:10px;" type="button"  class="alert alert-primary" role="alert">' + "Thanks for answering. We'll get back to you when we've processed it." + '</button>');
                $("#points-label").delay(2000).fadeOut('slow');
                // document.getElementById('points-label').innerHTML = "Thanks for answering. We'll get back to you when we've processed it.";
            }
            //points == null if it is a potential point
            else if (points == null) {


                $("#points-button").removeClass("alert-success");
                $("#points-button").removeClass("alert-warning");
                $("#points-button").removeClass("alert-danger");
                $("#points-button").addClass("alert-success");
                $('#points-label').show();
                document.getElementById('points-button').innerHTML = "<i class='fa fa-check' aria-hidden='true'></i> Thanks for answering. We'll get back to you when we've processed it.";
                $("#points-label").delay(2000).fadeOut('slow');


            } else if (points == 1)
            {
                $("#points-button").removeClass("alert-success");
                $("#points-button").removeClass("alert-warning");
                $("#points-button").removeClass("alert-danger");
                $("#points-button").addClass("alert-success");
                $('#points-label').show();
                document.getElementById('points-button').innerHTML = "<i class='fa fa-check' aria-hidden='true'></i> Good job! You've earned " + points + " point.";
                //$("#points-label").append('<button style="margin-right:10px; margin-top:10px;" type="button"  class="alert alert-primary" role="alert">' + "Good job! You've earned " + points + " point." + '</button>');
                $("#points-label").delay(2000).fadeOut('slow');
                // document.getElementById('points-label').innerHTML = "Good job! You've earned " + points + " point.";
            } else
            {
                $("#points-button").removeClass("alert-success");
                $("#points-button").removeClass("alert-warning");
                $("#points-button").removeClass("alert-danger");
                $("#points-button").addClass("alert-success");
                $('#points-label').show();
                document.getElementById('points-button').innerHTML = "<i class='fa fa-check' aria-hidden='true'></i> Good job! You've earned " + points + " points.";
                //$("#points-label").append('<button style="margin-right:10px; margin-top:10px;" type="button"  class="alert alert-primary" role="alert">' + "Good job! You've earned " + points + " points." + '</button>');
                $("#points-label").delay(2000).fadeOut('slow');
                //document.getElementById('points-label').innerHTML = "Good job! You've earned " + points + " points.";
            }
            // clean text field and get next question
            document.getElementById('answer-form').reset();
            eid = get_exercise(userid, 'RelatedTo');
            //$("#points-label").delay(4000).fadeOut('slow');
            //$("#question").val(randomresponse);
            // clean text field and get next question
            //        document.getElementById('answer-form').reset();
            //        eid = get_exercise(userid,'partof');

            //document.getElementById('question-label').innerHTML = exercise;
            // http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php?method=store_response&userid=1&eid=5&response=kolokotronis
        }
    });
});


$(document).on("click", "#yes", function (event)
{
    event.preventDefault();
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
        data: {
            'method': 'store_close_response',
            'userid': userid,
            'eid': eid,
            'response': '1'
        },
        beforeSend: function () {
        },
        success: function (data) {
            var points = data.points;
            // points==0 if it is a duplicate answer
            if ((points == 0) || (points < 0))
            {
                $("#points-button").removeClass("alert-success");
                $("#points-button").removeClass("alert-warning");
                $("#points-button").removeClass("alert-danger");
                $("#points-button").addClass("alert-danger");
                $('#points-label').show();

                document.getElementById('points-button').innerHTML = "<i class='fa fa-bug' aria-hidden='true'></i> Wrong! The correct answer was NO.";
                //$("#points-label").append('<button style="margin-right:10px; margin-top:10px;" type="button"  class="alert alert-primary" role="alert">' + "Thanks for answering. We'll get back to you when we've processed it." + '</button>');
                $("#points-label").delay(2000).fadeOut('slow');
                // document.getElementById('points-label').innerHTML = "Thanks for answering. We'll get back to you when we've processed it.";
            }
            //points == null if it is a potential point
            else if (points == null) {


                $("#points-button").removeClass("alert-success");
                $("#points-button").removeClass("alert-warning");
                $("#points-button").removeClass("alert-danger");
                $("#points-button").addClass("alert-success");
                $('#points-label').show();
                document.getElementById('points-button').innerHTML = "<i class='fa fa-check' aria-hidden='true'></i> Thanks for answering. We'll get back to you when we've processed it.";
                $("#points-label").delay(2000).fadeOut('slow');


            } else if (points == 1)
            {
                $("#points-button").removeClass("alert-success");
                $("#points-button").removeClass("alert-warning");
                $("#points-button").removeClass("alert-danger");
                $("#points-button").addClass("alert-success");
                $('#points-label').show();
                document.getElementById('points-button').innerHTML = "<i class='fa fa-check' aria-hidden='true'></i> Good job! You've earned " + points + " point.";
                //$("#points-label").append('<button style="margin-right:10px; margin-top:10px;" type="button"  class="alert alert-primary" role="alert">' + "Good job! You've earned " + points + " point." + '</button>');
                $("#points-label").delay(2000).fadeOut('slow');
                // document.getElementById('points-label').innerHTML = "Good job! You've earned " + points + " point.";
            } else
            {
                $("#points-button").removeClass("alert-success");
                $("#points-button").removeClass("alert-warning");
                $("#points-button").removeClass("alert-danger");
                $("#points-button").addClass("alert-success");
                $('#points-label').show();
                document.getElementById('points-button').innerHTML = "<i class='fa fa-check' aria-hidden='true'></i> Good job! You've earned " + points + " points.";
                //$("#points-label").append('<button style="margin-right:10px; margin-top:10px;" type="button"  class="alert alert-primary" role="alert">' + "Good job! You've earned " + points + " points." + '</button>');
                $("#points-label").delay(2000).fadeOut('slow');
                //document.getElementById('points-label').innerHTML = "Good job! You've earned " + points + " points.";
            }
            // clean text field and get next question
            document.getElementById('answer-form').reset();
            eid = get_exercise(userid, 'RelatedTo');
            //$("#points-label").delay(4000).fadeOut('slow');
            //$("#question").val(randomresponse);
            // clean text field and get next question
            //        document.getElementById('answer-form').reset();
            //        eid = get_exercise(userid,'partof');

            //document.getElementById('question-label').innerHTML = exercise;
            // http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php?method=store_response&userid=1&eid=5&response=kolokotronis
        }
    });
});

function reload(){
  location.reload();
}

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}


setInterval(function (event) {
    // retrieve points
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
        data: {
            'method': 'get_points',
            'userid': userid
                    //'etype': relation
                    // level
                    // relation
        },
        beforeSend: function () {
        },
        success: function (data) {
            points = data.summary.earned_points;
            // $('span#points-label').get(0).innerHTML = 'points';
            document.getElementById('score-label').remove();
            document.getElementById('user-label').remove();
            if (points == null) {
                points = 000
            }
            ;
            document.getElementById("ScoreExercise").innerHTML = "Your Score:  " +  points;
            $("#ScoreExercise").addClass("tooltip-inner");
            $("#mainNav").append('<div style="color:white;margin-left:120px" id="score-label">Current Points: ' + points + ' </div>');
            $("#mainNav").append('<div style="color:white;margin-left:120px" id="user-label">Username: ' + username + ' </div>');

            // event.preventDefault();
        }
    });
    // get notifications
    $.ajax({
        type: 'POST',
        crossDomain: true,
        url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
        data: {
            'method': 'get_notifications',
            'userid': userid
                    //'etype': relation
                    // level
                    // relation
        },
        beforeSend: function () {
        },
        success: function (data) {
            notifications = data.responses;
            if (Array.isArray(notifications) && notifications.length > 0)
            {
                var new_points = 0;
                notifications.forEach(function (item)
                {
                    new_points = new_points + item.points;
                });
                // why is this possible? API issue?
                if (new_points > 0)
                {
                    alert("You've earned " + new_points + " additional points through backend verification. Congrats!")
                }
            }

        }
    });


    // do stuff
}, 2000);
// //document.getElementById('points-label').onClick="function();return false;"
// //document.getElementById('points-label').innerHTML = "a lot";
// //$('span#points-label').get(0).innerHTML = 'a lot';
