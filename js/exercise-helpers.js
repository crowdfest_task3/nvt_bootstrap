// choose_exercise(uid)
// returns exercise_type = 'open' or 'close'


function get_exercise(userid, relation)
{
    $.ajax({
        type: 'POST',
        async: false,
        crossDomain: true,
        url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
        data: {
            'method': 'choose_exercise',
            'userid': userid
        },
        beforeSend: function () {
            $('#prev-res-label').hide();
        },
        success: function (data) {
            etype = data.exercise_type; // 'open' or 'close'
        }
    });

    if (etype == 'open')
    {
        $.ajax({
            type: 'POST',
            crossDomain: true,
            url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
            data: {
                'method': 'get_exercise',
                'userid': userid,
                'etype': relation
             //    'elevel': 'C1' // hardcoded for the moment
            },
            success: function (data) {
                var exercise = data.exercise;
                var prev_res = data.previous_responses;
                eid = data.eid;
                document.getElementById('question-label').innerHTML = exercise;
                $('#answer').show();
                $('#question').show();
                $('#yes').hide();
                $('#no').hide();
                if (prev_res.length>0) {
                    $('#prev-res-label').show();
                    document.getElementById('prev-res-button').innerHTML = "<i class='fa fa-info-circle' aria-hidden='true'></i> You have already answered: <span style='color:#4a41f4'><b>" + prev_res.join(", ")+"</b></span>";
                }

            }
        });
    }
    else if (etype == 'close')
    {
        $.ajax({
            type: 'POST',
            crossDomain: true,
            url: 'http://cognition-srv1.ouc.ac.cy/nvt/webservice/api.php',
            data: {
                'method': 'get_close_exercise',
                'userid': userid,
                'etype': relation
                //'elevel': 'C1' // hardcoded for the moment
            },
            success: function (data) {
                var exercise = data.exercise;
                var prev_res = data.previous_responses;
                eid = data.eid;
                document.getElementById('question-label').innerHTML = exercise;
                $('#answer').hide();
                $('#question').hide();
                $('#yes').show();
                $('#no').show();
                // if (prev_res.length>0) {
                //     $('#prev-res-label').show();
                //     document.getElementById('prev-res-button').innerHTML = "<i class='fa fa-info-circle' aria-hidden='true'></i> You have already answered: <span style='color:#4a41f4'><b>" + prev_res.join(", ")+"</b></span>";
                // }

            }
        });

    }
    return eid;
}


function generate_name()
{
    var uname;
    $.ajax({
        type: 'GET',
        async: false,
        crossDomain: true,
        url: 'https://frightanic.com/goodies_content/docker-names.php',
        beforeSend: function () {
        },
        success: function (data) {
            //alert(data);
            uname = data.trim();
            //alert("data: " + uname);
        },
        error: function (error) {
            alert("Cannot get data");
        }
    });
    return uname;
}
